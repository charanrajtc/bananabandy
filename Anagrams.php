<?php 
/*
	Anagrams php class
	@author charanraj.tc@gmail.com
*/


/*
	Anagrams php class
*/
class AreAnagrams
{
 
	 /*
		Function test strings are Anagrams 
		Time complexity log n
		Retrun true or false 
		@param $a String type ,$b String type 
		@return boolean 
	  */
	 public static function areStringsAnagrams($a, $b)
	 {
	 	// check the $a and $b are of type string 
	 	if(!is_string($a) || !is_string($b))
	 		return false;

	 	// string length for the comparision
	 	$lena=strlen($a);
	 	$lenb=strlen($b);

	 	// length of the strings are not equal 
	 	if($lena!=$lenb)
	 		return false;
	 	
	 	for ($i=0; $i < $lena ; $i++) { 
	 		// Debug line
	 		// echo $a[$i]." : ".$b[($lena-1)-$i]."</br>";
	 		if($a[$i]!=$b[($lena-1)-$i]){
	 			// if any one the characters doesn't match then return false  
				return false;
	 		}
	 	}
 		// return true string are anagrams
 		return true;
 	}
}

// For testing purposes

echo "momdad : dadmom = ";
echo AreAnagrams::areStringsAnagrams('momdad', 'dadmom') ? 'True' : 'False';
echo "</br>";
echo "momdad : 2 = ";
echo AreAnagrams::areStringsAnagrams('momdad', 2) ? 'True' : 'False';
echo "</br>";
echo "2 : momdad = ";
echo AreAnagrams::areStringsAnagrams( 2,'momdad') ? 'True' : 'False';
echo "</br>";
echo "2 : 2.4  = ";
echo AreAnagrams::areStringsAnagrams( 2, 2.4) ? 'True' : 'False';
echo "</br>";
echo "momdas : dadmom = ";
echo AreAnagrams::areStringsAnagrams('momdas', 'dadmom') ? 'True' : 'False';