<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=coresoft.co.in;dbname=bananaBandy',
            'username' => 'bananaBandy',
            'password' => 'bananaBandy',
            'charset' => 'utf8',
        ],
    ],
];
