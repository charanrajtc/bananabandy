<?php
namespace backend\models;

use common\models\User;

use yii\base\Model;
use Yii;

/**
 * updateuser form
 */
class UpdateUserForm extends Model
{
   
    // public $email;
    public $password;
    public $role;
    // public $username;
    public $id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                        
            ['role','required'],            
            ['role', 'filter', 'filter' => 'trim'],
            ['role', 'in', 'range' => User::$ROLES ],

            // ['password', 'required'],
            ['password', 'string', 'min' => 0],   
            ['password', 'filter', 'filter' => 'trim'],    
            [['id'],'safe']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function save($user)
    {
        if ($this->validate()) {
            $user->role = $this->role;
            if($this->password!=""){
                 $user->setPassword($this->password);  
             }           
            if($user->save()) { 
                  // the assign the roles to the user 
                $auth = Yii::$app->authManager;
                $userRole = $auth->getRole($user->role);
                $auth->revokeAll($user->getId());
                $auth->assign($userRole, $user->getId());

                return $user;                
            }
        } 
        return null;
    }
}