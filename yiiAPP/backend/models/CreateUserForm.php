<?php
namespace backend\models;

use common\models\User;

use yii\base\Model;
use Yii;

/**
 * Createuser form
 */
class CreateUserForm extends Model
{
   
    public $email;
    public $password;
    public $role;
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
    
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
           
            ['role','required'],            
            ['role', 'filter', 'filter' => 'trim'],
            ['role', 'in', 'range' => User::$ROLES ],

            ['password', 'required'],
            ['password', 'string', 'min' => 0],
            ['password', 'filter', 'filter' => 'trim'],    

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            // ['username', 'username'],
            ['username', 'string', 'max' => 255],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username address has already been taken.'],      
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();           
            $user->email = $this->email;
            $user->role = $this->role;
            $user->username=$this->username;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            //set trail period to true           
            if($user->save()) {                 
                // the assign the roles to the user 
                $auth = Yii::$app->authManager;
                $userRole = $auth->getRole($user->role);
                $auth->assign($userRole, $user->getId());
                return $user;                
            }
        } 
        return null;
    }

    /**
     * Signs user up without authorizing the DB 
     *
     * @return User|null the saved model or null if saving fails
     */
    public function insertDB()
    {
        if ($this->validate()) {
            $user = new User();           
            $user->email = $this->email;
            $user->role = $this->role;
            $user->username=$this->username;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            //set trail period to true           
            if($user->save()) {    
                return $user;                
            }
        } 
        return null;
    }
}