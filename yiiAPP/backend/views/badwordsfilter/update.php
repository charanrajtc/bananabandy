<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Badwordsfilter */

$this->title = 'Update Badwordsfilter: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Badwordsfilters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="badwordsfilter-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
