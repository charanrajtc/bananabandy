<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Badwordsfilter */

$this->title = 'Create Badwordsfilter';
$this->params['breadcrumbs'][] = ['label' => 'Badwordsfilters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="badwordsfilter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
