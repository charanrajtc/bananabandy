<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h2><?= Html::encode($this->title) ?></h2>
    <hr>

    <p>Please fill out the following fields to signup </p>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?= $form->field($model, 'email') ?>
                 <?= $form->field($model, 'username') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                  <?= $form->field($model, 'role')->dropDownList(['user'=>'Normal User ','privileged'=>'Privileged user','admin'=>'Administrator']); ?>
                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>