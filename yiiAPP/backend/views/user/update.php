<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Update User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php $form = ActiveForm::begin(['id' => 'form-update']); ?>              
                <?= $form->field($model, 'password')->passwordInput() ?>
                 <?= $form->field($model, 'role')->dropDownList(['user'=>'Normal User ','privileged'=>'Privileged user','admin'=>'Administrator']); ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
