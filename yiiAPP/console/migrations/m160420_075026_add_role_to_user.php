<?php

use yii\db\Migration;

class m160420_075026_add_role_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role', $this->string(45)->notNull()->defaultValue('user'));
    }

    public function down()
    {
        $this->dropColumn('user', 'role');
    }
}
