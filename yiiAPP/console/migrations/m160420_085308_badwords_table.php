<?php

use yii\db\Migration;

class m160420_085308_badwords_table extends Migration
{
    public function up()
    {
        // create words filter table and the fields are self explainatory 
         $this->createTable('badwordsfilter', [
            'id' => $this->primaryKey(),
            'word' => $this->string(45)->notNull(),            
            'created_at' => $this->datetime(),
            'updated_at' =>  $this->datetime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('badwordsfilter');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
