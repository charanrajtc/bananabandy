<?php

use yii\db\Migration;

class m160418_071641_message_table extends Migration
{
    public function up()
    {   
        // create message table and the fields are self explainatory 
         $this->createTable('message', [
            'id' => $this->primaryKey(),
            'sender_user_id' => $this->integer()->notNull(),
            'receiver_user_id' => $this->integer()->notNull(),
            'message_text' => $this->text()->notNull(),
            'created_at' => $this->datetime(),
            'updated_at' =>  $this->datetime(),
        ]);

         // creates index for column `sender_user_id`
        $this->createIndex(
            'idx-message-sender_user_id',
            'message',
            'sender_user_id'
        );

        // creates index for column `receiver_user_id`
        $this->createIndex(
            'idx-message-receiver_user_id',
            'message',
            'receiver_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-message-sender_user_id',
            'message',
            'sender_user_id',
            'user',
            'id',
            'CASCADE'
        );  

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-message-receiver_user_id',
            'message',
            'receiver_user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        /* 
           Default Code for the down function 
           echo "m160418_071641_message_table cannot be reverted.\n";
           return false;
        */
       
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-message-sender_user_id',
            'message'
        );

        // drops index for column `sender_user_id`
        $this->dropIndex(
            'idx-message-sender_user_id',
            'message'
        );

        // drops foreign key for table `category`
        $this->dropForeignKey(
            'fk-message-receiver_user_id',
            'message'
        );

        // drops index for column `receiver_user_id`
        $this->dropIndex(
            'idx-message-receiver_user_id',
            'message'
        );

        $this->dropTable('message');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
