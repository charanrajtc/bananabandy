<?php

use yii\db\Migration;

class m160418_123829_add_isred_to_message extends Migration
{
    public function up()
    {
        $this->addColumn('message', 'isred', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('message', 'isred');
    }
}
