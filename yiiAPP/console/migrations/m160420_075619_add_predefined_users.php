<?php

use yii\db\Migration;
use backend\models\CreateUserForm;
use common\models\User;

class m160420_075619_add_predefined_users extends Migration
{
    public function up()
    {
        //sample user 
        $joeUser=new CreateUserForm();
        $joeUser->username='joeUser';
        $joeUser->password='joe';
        $joeUser->email='joe@tempinbox.com';
        $joeUser->role=User::ROLE_USER;
        $joeUser->insertDB();

        // sample privaled user 
        $anniePrivileged=new CreateUserForm();
        $anniePrivileged->username='anniePrivileged';
        $anniePrivileged->password='annie';
        $anniePrivileged->email='annie@tempinbox.com';
        $anniePrivileged->role=User::ROLE_PRIVILEGED;
        $anniePrivileged->insertDB();

        //sample admin user  
        $robAdmin=new CreateUserForm();
        $robAdmin->username='robAdmin';
        $robAdmin->password='rob';
        $robAdmin->email='rob@tempinbox.com';
        $robAdmin->role=User::ROLE_ADMIN;
        $robAdmin->insertDB();

    }

    public function down()
    {
        $joeUser=User::findByUsername('joeUser');
        if($joeUser){
             $joeUser->delete();
         }       
        $anniePrivileged=User::findByUsername('anniePrivileged');
        if($anniePrivileged){
            $anniePrivileged->delete();
        }
        $robAdmin=User::findByUsername('robAdmin');
        if($robAdmin){
            $robAdmin->delete();
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
