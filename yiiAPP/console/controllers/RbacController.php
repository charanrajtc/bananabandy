<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\User;
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // add "curdUser" permission READ,EDIT,DELETE,CREATE OPERATIONS operations on user 
        $curdUser = $auth->createPermission('curdUser');
        $curdUser->description = 'READ,EDIT,DELETE,CREATE OPERATIONS operations on user  table';
        $auth->add($curdUser);

        // add "curdBadwordsfilter" permission  READ,EDIT,DELETE,CREATE OPERATIONS operations on Badwordsfilter 
        $curdBadwordsfilter = $auth->createPermission('curdBadwordsfilter');
        $curdBadwordsfilter->description = 'READ,EDIT,DELETE,CREATE OPERATIONS operations on Badwordsfilter table ';
        $auth->add($curdBadwordsfilter);


         // add "curdMessage" permission  READ,EDIT,DELETE,CREATE OPERATIONS operations on Message 
        $curdMessage = $auth->createPermission('curdMessage');
        $curdMessage->description = 'READ,EDIT,DELETE,CREATE OPERATIONS operations on Message table ';
        $auth->add($curdMessage);

        // add "allowBadwordsInMessage" permission  to use bad words in the Message 
        $allowBadwordsInMessage = $auth->createPermission('allowBadwordsInMessage');
        $allowBadwordsInMessage->description = 'permission  to use bad words in the Message ';
        $auth->add($allowBadwordsInMessage);

        // add "user" role and give this role the "curdMessage" permission
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $curdMessage);

        // add "privileged" role and give this role the "allowBadwordsInMessage" permission
        // as well as the permissions of the "user" role
        $privileged = $auth->createRole('privileged');
        $auth->add($privileged);
        $auth->addChild($privileged, $user);
        $auth->addChild($privileged, $allowBadwordsInMessage);

        // add "admin" role and give this role the "curdUser" and "curdBadwordsfilter" permission
        // as well as the permissions of the "privileged" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $curdUser);
        $auth->addChild($admin, $curdBadwordsfilter);
        $auth->addChild($admin, $privileged);

        // Assign roles to users. in the existing database users       
        $models= User::find()->all();
        foreach ($models as $key => $model) {
            if($model->role=="user"){
                $auth->assign($user,$model->id);
            }            
            if($model->role=="privileged"){
                $auth->assign($privileged,$model->id);
            }
            if($model->role=="admin"){
                $auth->assign($admin,$model->id);
            }         
        }      
    }

    public function actionDown()
    {   
        //removes all users and roles and autorization data 
        $manager = \Yii::$app->authManager;
        $manager->removeAll();
    }
}