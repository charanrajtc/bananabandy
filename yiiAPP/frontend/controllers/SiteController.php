<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\web\Response;
use \yii\db\Query;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [                    
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }    

    public function actionGetNotificationsMessages($authkey=0)
     {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $authkey=isset($_REQUEST['authkey']) ? $_REQUEST['authkey'] : 0;
                    
             if ($user_id=User::validateAjaxAuth($authkey)) {

                $user=User::findOne(['id'=>$user_id]);
                $message=array();
                
                if (Yii::$app->request->isPost) {
                    
                    
                     // select * from messages where receiver_user_id=1 and (sent_at=recieved_at) order by id DESC limit 10 ;
                    $query = (new Query())
                        // ->select('count(id)') 
                        ->from(['message'])
                        ->where( ' ( receiver_user_id=:useridto ) and  (isred=0) ',[':useridto'=>$user->id])
                        ->orderBy(['id'=>SORT_DESC]);
                    $count=$query->count();                         
                    $res = array(
                            'message'   => array('count'=>$count) ,
                            'status' =>200,
                        );
                                            
                }else
                {
                    throw new \yii\web\HttpException(500, ' Make Post Call to get Messages  Noftiications ');   
                }

                return $res;
            }else
            {
                throw new \yii\web\HttpException(403, ' Damm you are not allowed perform this action'); 
            }

        }
        
        //message auto fill 
        public function actionAutofillUsers($authkey=0) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $authkey=isset($_REQUEST['authkey']) ? $_REQUEST['authkey'] : 0;
                    
             if ($user_id=User::validateAjaxAuth($authkey)) {

                $user=User::findOne(['id'=>$user_id]);
                $message=array();
                
                if (Yii::$app->request->isPost) {
                    
                    $pageno=isset($_REQUEST['pageno']) ? $_REQUEST['pageno'] : 0 ;
                    $like=isset($_REQUEST['like']) ? trim($_REQUEST['like']) : "" ;
                    
                    if(is_null(trim($pageno)))
                    {
                        throw new UserException(" Page No cannot be Null  ");
                    }

                    if (!$this->isonlynumbers($pageno)) {
                        throw new UserException(" Page No should be number only ");
                    }   

                    

                    $query = (new Query()) ->select(['user.username as id','user.username as name'])
                        ->from( "user ");


                    // $query->where(['like','Tprofile.first_name','kar']);
                    if($like!=""){
                        $query->where(['like','user.username',$like]);
                        // $query->where(['or',['or' ,['like','user_profile.first_name',$like],['like','user_profile.last_name',$like]]]); 
                    }else {
                        $query->where(['not', ['id' => $user->id]]);
                    }   
                    // 
                    
                    // $query->leftJoin('user_profile','user_profile.user_id=user.id');

                    $command = $query->createCommand()->getSql();

                                     
                    $pagesize = 20;
                    $totalrowcount=$query->count();
                    if(!$query->exists())
                    {
                        throw new UserException(" No users conected  ");
                        
                    }

                    $totalrowcount = isset($totalrowcount) ? (int) $totalrowcount : 0;
                    // var_dump($totalrowcount);
                    $totalpagecount = (int) round($totalrowcount / $pagesize);
                    $offset = $pageno * $pagesize;
                    $offset = $offset < 0 ? 0 : $offset;
                    $offset += 1;
                    $limit = $pagesize;

                    $query->limit($limit);
                    if($pageno!=0)
                    {
                        $query->offset($offset);    
                    }
                    $command = $query->createCommand()->getSql();
                    $models=$query->all();
                

                    $res = array(
                              'message'   => array('data'=>$models,'totalpagecount'=>$totalpagecount,'pageno'=>$pageno) ,
                            'status' =>200,
                        );
                    // 'limit' => $limit, 'offset' => $offset)

                                    
                }else
                {
                    throw new \yii\web\HttpException(500, ' Make Post Call to get Messages  '); 
                }

                return $res;
            }else
            {
                throw new \yii\web\HttpException(403, ' Damm you are not allowed perform this action'); 
            }
        }

        /**
        * Validate that the value is an integer. 
        * @param mixed val 
        * @return true or false
        */
        public static function isonlynumbers($value)
        {
            if(preg_match('/[^0-9]/',$value))
                return false;
            else
                return true;
        }
}
