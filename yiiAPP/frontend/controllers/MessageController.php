<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Message;
use frontend\models\MessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use yii\helpers\Url;
use common\models\BadwordsfilterSearch;

/**
 * MessageController implements the CRUD actions for Message model.
 */
class MessageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),  
                'only' => ['delete'],              
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],                 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndex()
    {
        $count = Message::find()->where(['isred' => 0, 'receiver_user_id' => Yii::$app->user->id])->count();
        $searchModel = new MessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=5;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,'count'=>$count,
        ]);
    }

    public function actionSent()
    {
        $count = Message::find()->where(['isred' => 0, 'receiver_user_id' => Yii::$app->user->id])->count();
        $searchModel = new MessageSearch();
        $dataProvider = $searchModel->searchsentmessage(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=5;

        return $this->render('sent', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'count'=>$count,
        ]);
    }

    /**
     * Displays a single Message model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $count = Message::find()->where(['isred' => 0, 'receiver_user_id' => Yii::$app->user->id])->count();
        
        $update=Yii::$app->db->createCommand()
        ->update('message', ['isred' => 1], 'id ="'.$id.'"')
        ->execute();
        $data = User::findOne($id);
        $model=$this->findModel($id);
        $user = User::find()->where('id = :userid', [':userid' =>$model->sender_user_id]) ->one();
        $userreceiver = User::find()->where('id = :userid', [':userid' =>$model->receiver_user_id]) ->one();

        return $this->render('view', [
            'model' => $model,
            'data' =>$data, 'count'=>$count,'user'=>$user,'userreceiver'=>$userreceiver,
        ]);
    }

    /**
     * Creates a new Message model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Message();
        $user = new User();
        //notification count 
        $count = Message::find()->where(['isred' => 0, 'receiver_user_id' => Yii::$app->user->id])->count();
        if($id != null)
        {
            $user = User::find('username')->where('id = :userid', [':userid' => $id]) ->one();
        }
        $model->sender_user_id = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
           
            $data = User::findOne(['username'=>$model->username]);
            if($data!=null && $data->id>0)
            $model->receiver_user_id= $data->id;        
            if($model->receiver_user_id==$model->sender_user_id)
                throw new \yii\web\HttpException(500, 'you cannot send the message to yourself');
            
            // serach the message for any bad words filter 
            $searchModelBadwords = new BadwordsfilterSearch();
            $dataProviderBadwords = $searchModelBadwords->fulltextsearch($model->message_text);

            // has bad words and user dont have permission to send messages throw an error 
            if($dataProviderBadwords->getTotalCount() > 0 && ! Yii::$app->user->can('allowBadwordsInMessage')){
                // log events 
                $model->trigger(Message::EVENT_MESSAGE_BADWORDS_DENIED); 
                throw new \yii\web\HttpException(403, 'You cannot send the message with bad words ');
            }
           
            if($model->save())
            {   
                // log events 
                $model->trigger(Message::EVENT_MESSAGE_SENT); 
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else
            {
                $tousername = User::find('username')->where('id = :userid', [':userid' => $data->id]) ->one();
                if($tousername)
                    $tousername=$tousername->username;
                else
                    $tousername=null;
                // $tousername="s";
                return $this->render('create', [
                    'model' => $model,'count'=>$count,'tousername'=>$tousername,
                ]);
            }             
            
        } else {
            $model->username=$user->username;
            $tousername = User::find('username')->where('id = :userid', [':userid' => $id]) ->one();       
            if($tousername)
                $tousername=$tousername->username;
            else
                $tousername=null;
            return $this->render('create', [
                'model' => $model,'count'=>$count,'tousername'=>$tousername,
            ]);
        }
    }


    /**
     * Deletes an existing Message model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    

    /**
     * Finds the Message model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }  

}