<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
AppAsset::register($this);
$authkey="dummyNull";
if(!Yii::$app->user->isGuest) {
    $user = \common\models\User::findOne(['id' => Yii::$app->user->id]);
    $authkey=$user->auth_key;  
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Bananabandy (User)',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-blue navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],       
    ];
    if (Yii::$app->user->isGuest) {       
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];      
    } else {       
         //message link
         $menuItems[] = '<li><a href="'.Url::toRoute('message/index').'" class="nav-btn messageshref"> Messages <span class="badge">0</span> </a></li>';
          $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    echo  '<span class="hidden" id="authkey">'.$authkey.'</span>';
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Bananabandy (User) <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

<?php if(!Yii::$app->user->isGuest) { ?>
<script type="text/javascript">
    //message notification list 
    // http://localhost/WorkspaceAngle/defy/defy/frontend/web/index.php?r=ajax/get-notifications-messages
    $(document).ready(function() {
              function fetchmessagenotifications() {
                        //fetching chat heads 
                        var fd = new FormData();
                        //input the from data please follow the order as below only 
                        fd.append('authkey','<?php echo $authkey;?>');                        
                        $.ajax({
                            type: 'POST',
                            url: '<?=Url::toRoute('site/get-notifications-messages') ?>',
                            dataType: 'json',
                            processData: false, // Not supported with Trigger
                            contentType: false, // Not supported with Trigger
                            data: fd,
                            error: function(response) {
                                // console.log(response.responseText);
                                 console.log("error in fetch notifications");
                            },
                            success: function(response) {
                               
                                if(response.status=200)
                                {
                                   if(response.message.count > 0)
                                    $(".messageshref").html(" Messages <span class='badge'>"+response.message.count +"</span>");
                                   else
                                    $(".messageshref").html(" Messages ");

                                }
                            }

                        });
                    }                         

            var t;
            var timer_is_on = 0;

            function timedCount() {
               fetchmessagenotifications();
                t = setTimeout(function(){timedCount()}, 10000);
            }

            function doTimer() {
                if (!timer_is_on) {
                    timer_is_on = 1;
                    timedCount();
                }
            }

            function stopCount() {
                clearTimeout(t);
                timer_is_on = 0;
            }

            doTimer();

    });        
</script>
<?php } ?>
</body>
</html>
<?php $this->endPage() ?>
