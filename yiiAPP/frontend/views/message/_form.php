<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\web\View;
use yii\helpers\Url;
use yii\jui\JuiAsset;
JuiAsset::register($this);

/* @var $this yii\web\View */
/* @var $model frontend\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php

    $dataList=ArrayHelper::map(User::find()->asArray()->all(), 'username','username');
   /* <?=$form->field($model, 'username')->label('Send To ')->dropDownList($dataList, 
         ['prompt'=>'-Choose a username-']) ?> */

?>
<?php $this->registerJsFile('//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?> 
<?php $this->registerJsFile('//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?> 

<div class="message-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if($model->username== null || $model->hasErrors('username'))
    {   
        ?>
        
        
        <?=Html::activeHiddenInput($model, 'username') ?>
        <?php if($model->hasErrors('username')) { ?>
        <div class="form-group  required has-error" id="newmessageusernamemodalform">
        <label class="control-label" for="message-name">Send To </label>
                <input type="text" class="form-control" id="newmessageusernamemodal" placeholder="Enter the name Slowly">
            <div id="newmessageusernamemodalhelp" class="help-block">This user does not exist.</div>
        </div>
        <?php }else { ?>
            <div class="form-group  required ">
                <label class="control-label" for="message-name">Send To </label>
                <input type="text" name="message-name" class="form-control" id="newmessageusernamemodal" placeholder="Enter the name Slowly">
                
            </div>
        <?php } ?>


    <?php }
    else
    {
        // $tousername ="s";
        if($tousername)
        {
            echo '<small>To: '.$tousername.'</small>';
        } 
        ?>

        <?=Html::activeHiddenInput($model, 'username') ?>
    <?php } ?>
    
    <?= $form->field($model, 'message_text')->label('Message')->textarea(['rows' => 6, 'placeholder'=>'Message Body']) ?>
<style type="text/css">
    #message-message_text {
        color: black;
    }
</style>
    <div class="row">
        <div class="col-lg-12">
            <div class="btn-toolbar message-controls" role="toolbar">
                <div class="pull-right">
                    <button class="btn btn-transparent"> <span>Send</span> <i class="fa fa-send"></i> </button>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('$( document ).ready(function() {

     // console.log("hello");
  
        String.prototype.trunc =
            function(n, useWordBoundary) {
                var toLong = this.length > n,
                    s_ = toLong ? this.substr(0, n - 1) : this;
                s_ = useWordBoundary && toLong ? s_.substr(0, s_.lastIndexOf(" ")) : s_;
                return toLong ? s_ + "&hellip;" : s_;
            };
             $( "#newmessageusernamemodal" ).autocomplete({
            _renderItem:function(ul,item){
                return $( "<li>" )
                         .attr( "data-id", item.id )
                         .append( item.label )
                         .appendTo( ul );

                    },
            select: function( event, ui ) {
                $( "#newmessageusernamemodal" ).val(ui.item.label);
                $( "#message-username" ).val(ui.item.id);


            },
            change: function(event,ui)
                    {
                        if (ui.item==null)
                        {
                            $("#newmessageusernamemodal").val("");
                            $( "#message-username" ).val(0);
                            $("#newmessageusernamemodal").focus();
                        }
                    },
            source: function( request, response ) {
                pageno=0;
                var fd = new FormData();
                //input the from data please follow the order as below only 
                fd.append("authkey", $("#authkey").html());
                fd.append("pageno", pageno);
                fd.append("like",request.term.trim());

                var re = new RegExp(/^[a-zA-Z0-9\s]+$/);
                    if(!re.test(request.term)) {
                        $( "#newmessageusernamemodal" ).val("");
                        $( "#message-username" ).val(0);
                        responseautocomplete(); 
                        return;
                    } 
                var responseautocomplete=response;
                $.ajax({
                    type: "POST",
                    url: "'.Url::toRoute("site/autofill-users").'",
                    dataType: "json",
                    processData: false, // Not supported with Trigger
                    contentType: false, // Not supported with Trigger
                    data: fd,
                    error: function(response) {
                        responseautocomplete();
                        $( "#message-username" ).val(0);
                    },
                    success: function(response) {
                        if(response.status==200)
                        {
                            var tags = [];
                            for (var i = response.message.data.length - 1; i >= 0; i--) {
                                tags[i]={ label: response.message.data[i].name , value: response.message.data[i].name , id:response.message.data[i].id , image : response.message.data[i].img };
                            }
                            responseautocomplete(tags);

                        }else
                        {
                            responseautocomplete(); 
                            $( "#message-username" ).val(0);
                        }
                        
                    }

                });
               
                }
        });
    $("#newmessageusernamemodal").change(function(event){
        if(String($(this).val).trunc()=="")
        {
          $( "#message-username" ).val(0);
          $("#newmessageusernamemodalform" ).addClass("has-error");
          $("#newmessageusernamemodalhelp").html("User Name required ");

        }
        $( "#newmessageusernamemodalform" ).removeClass("has-error");
        $("#newmessageusernamemodalhelp").html("");

    });

    // $(".form-group.field-message-username").hide();

     jQuery.validator.setDefaults({        
      highlight: function(element) {
            jQuery(element).closest(".form-group").addClass("has-error");
            // jQuery(element).closest(".form-group").removeClass("has-success");
        },
        unhighlight: function(element) {
            jQuery(element).closest(".form-group").removeClass("has-error");
            // jQuery(element).closest(".form-group").addClass("has-success");
        },
        errorElement: "div",
        errorClass: "help-block",
        errorPlacement: function(error, element) {
            console.log(element.attributes);
            if(element.parent(".input-group").length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#w0").validate({
      //   submitHandler: function(form) {
      //   // form.submit();
      // },
      ignore: "not:hidden",
      rules: {        
        "message-name": { 
            required : true,
        },
        "Message[message_text]": { 
            required : true,
        },

      },
      messages :{
        "message-name" : {
            required :"  Please Enter Username  "
        },  
        "Message[message_text]": { 
            required : "Message is required ",
        },   
      }
    });
           }); ',View::POS_READY,'sendmessage-js'); ?> 