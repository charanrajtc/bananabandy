<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use frontend\components\CustomPagination;
use frontend\models\Message;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sent Messages';
$this->params['breadcrumbs'][] = $this->title;

?>
        <!-- start here -->
        <!-- Page-Title -->
         <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Sent</h4>
                <p>
                Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Nunc sed turpis. Vivamus merfio ljudyio hferhu euismod mauris. Curabitur nisi.
                </p>
                <hr>
            </div>
        </div>
        <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-lg-3 col-md-4">
                        <div class="panel panel-default p-0  m-t-20">
                            <div class="list-group mail-list">
                                <a href="<?= Url::to(['create','id'=>""])?>" class="list-group-item no-border"><i class="fa fa-pencil m-r-5"></i> &nbsp;&nbsp;Compose  </a>
                                <a href="<?= Url::to(['index'])?>" class="list-group-item no-border "><i class="fa fa-download m-r-5"></i> &nbsp;&nbsp; Inbox <b> <?= Html::encode('('.$count.')') ?></b></a>
                                <!-- <a href="#" class="list-group-item no-border"><i class="fa fa-star-o m-r-5"></i> &nbsp;&nbsp; Starred</a> -->
                                <!-- <a href="#" class="list-group-item no-border"><i class="fa fa-file-text-o m-r-5"></i> &nbsp;&nbsp; Draft <b>(20)</b></a> -->
                                <a href="<?= Url::to(['sent'])?>" class="list-group-item no-border active"><i class="fa fa-paper-plane-o m-r-5"></i> &nbsp;&nbsp; Sent Mail</a>
                                <!-- <a href="#" class="list-group-item no-border"><i class="fa fa-trash-o m-r-5"></i> &nbsp;&nbsp; Trash <b>(354)</b></a> -->
                            </div>
                        </div>
                    </div>
                    <!-- End Left sidebar -->

                    <!-- Right Sidebar -->
                    <div class="col-lg-9 col-md-8">
                        <div class="panel panel-default m-t-20">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'tableOptions' => 
                                    [
                                        'class' => 'table table-hover mails'
                                    ],
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    [   
                                        'label'=>'To',
                                        'attribute'=>'receiver_user_id',
                                        'format' => 'raw',
                                           'value'=>function ($data) {
                                            $query = (new \yii\db\Query())->select('*')->from('user')->where(['id' => $data->receiver_user_id]);
                                            $command = $query->createCommand();
                                            $model= $command->queryAll();
                                            foreach($model as $row) {
                                                $username = $row['username'];
                                            }
                                            return Html::a( $username, ['create','id'=>$data->receiver_user_id]);
                                            
                                        },
                                    ],

                                    [   
                                        'label'=>'Message',
                                        'attribute'=>'message_text',
                                        'format' => 'raw',
                                        'value'=>function ($data) {
                                           $message= substr($data->message_text,0, 50);                                        
                                           return Html::a( $message, ['view','id'=>$data->id]);
                                        },
                                    ],

                                    [   
                                        'label'=>'Sent At',
                                        'attribute'=>'created_at',
                                        'format' => 'raw',
                                        'value'=>
                                            function ($data) {
                                                $now = new DateTime();
                                                $selecteddate=date("d/m/y ",  strtotime($data->created_at));
                                                $currentdate=$now->format('d/m/y');
                                                if($currentdate > $selecteddate){
                                                    return date("d M ",  strtotime($data->created_at));
                                                }
                                                else
                                                {
                                                     return date(" H:i A",  strtotime($data->created_at));
                                                }
                                           },
                                    ],
                                ],
                            ]); ?>


                                </div>
                                <hr>
                                              <?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/messagejquery.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?> 
                                <div class="row">
                                    <div class="col-xs-7" id="custom_summary">
                                    </div>
                                    <div class="col-xs-5">
                                        <div class="btn-group pull-right" id="custompagination">
                                            <button type="button" class="btn btn-green" id="prev"><i class="fa fa-chevron-left"></i></button>
                                            <button type="button" class="btn btn-green" id="next"><i class="fa fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- panel body -->
                        </div>
                        <!-- panel -->
                    </div>
                    <!-- end Col-9 -->
                </div>
