<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use frontend\components\CustomPagination;
use frontend\models\Message;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Create Message';
$this->params['breadcrumbs'][] = $this->title;
if(!isset($tousername))
    $tousername=null;
?>

        <!-- start here -->
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Create Message</h4>
                <p>
                Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Nunc sed turpis. Vivamus merfio ljudyio hferhu euismod mauris. Curabitur nisi.
                </p>
                <hr>
            </div>
        </div>
        <div class="row">
            <!-- Left sidebar -->
            <div class="col-lg-3 col-md-4">
                <div class="panel panel-default p-0  m-t-20">
                    <div class="list-group mail-list">
                        <a href="<?= Url::to(['create','id'=>""])?>" class="list-group-item no-border active"><i class="fa fa-pencil m-r-5"></i> &nbsp;&nbsp;Compose  </a>
                        <a href="<?= Url::to(['index'])?>" class="list-group-item no-border "><i class="fa fa-download m-r-5"></i> &nbsp;&nbsp; Inbox <b> <?= Html::encode('('.$count.')') ?></b></a>
                        <!-- <a href="#" class="list-group-item no-border"><i class="fa fa-star-o m-r-5"></i> &nbsp;&nbsp; Starred</a> -->
                        <!-- <a href="#" class="list-group-item no-border"><i class="fa fa-file-text-o m-r-5"></i> &nbsp;&nbsp; Draft <b>(20)</b></a> -->
                        <a href="<?= Url::to(['sent'])?>" class="list-group-item no-border" ><i class="fa fa-paper-plane-o m-r-5"></i> &nbsp;&nbsp; Sent Mail</a>
                        <!-- <a href="#" class="list-group-item no-border"><i class="fa fa-trash-o m-r-5"></i> &nbsp;&nbsp; Trash <b>(354)</b></a> -->
                    </div>
                </div>
            </div>
             <!-- End Left sidebar -->
            <!-- Right Sidebar -->
            <div class="col-md-8 col-lg-9">
                <div class="panel panel-default m-t-20">
                    <div class="panel-body p-t-30">
                         <?= $this->render('_form', [
					        'model' => $model,'tousername'=>$tousername,
					    ]) ?>
                    </div>
                    <!-- panel -body -->
                </div>
                <!-- panel -->
            </div>
            <!-- end Col-9 -->
        </div>
