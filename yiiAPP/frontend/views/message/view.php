<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use common\models\UserProfile;
use frontend\models\Message;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Message */
$this->title =  ' View Message ' ;
$this->params['breadcrumbs'][] = ['label' => 'Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/**
 * @var $user
 * @var $userreceiver
 */
?>
<!-- start here -->
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">View Message</h4>
        <p>
            Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Nunc sed turpis. Vivamus merfio ljudyio hferhu euismod mauris. Curabitur nisi.
        </p>
        <hr>
    </div>
</div>
<div class="row">
    <!-- Left sidebar -->
    <div class="col-lg-3 col-md-4">
        <div class="panel panel-default p-0  m-t-20">
            <div class="list-group mail-list">
                <a href="<?= Url::to(['create','id'=>""])?>" class="list-group-item no-border "><i class="fa fa-pencil m-r-5"></i> &nbsp;&nbsp;Compose  </a>
                
                <?php if(Yii::$app->user->id == $model->receiver_user_id)
                    { ?>

                        <a href="<?= Url::to(['index'])?>" class="list-group-item no-border active"><i class="fa fa-download m-r-5"></i> &nbsp;&nbsp; Inbox <b> <?= Html::encode('('.$count.')') ?></b></a>
                        <a href="<?= Url::to(['sent'])?>" class="list-group-item no-border"><i class="fa fa-paper-plane-o m-r-5"></i> &nbsp;&nbsp; Sent Mail</a>
                 
                 <?php   }  else { ?>

                        <a href="<?= Url::to(['index'])?>" class="list-group-item no-border "><i class="fa fa-download m-r-5"></i> &nbsp;&nbsp; Inbox <b> <?= Html::encode('('.$count.')') ?></b></a>
                        <a href="<?= Url::to(['sent'])?>" class="list-group-item no-border active"><i class="fa fa-paper-plane-o m-r-5"></i> &nbsp;&nbsp; Sent Mail</a>
                
                 <?php   }  ?>
                
            </div>
        </div>
    </div>
     <!-- End Left sidebar -->
    <!-- Right Sidebar -->
    <div class="col-md-8 col-lg-9">
        <div class="panel panel-default m-t-20">
            <div class="panel-heading">
                <h3 class="panel-title">
                <?php if(Yii::$app->user->id == $model->receiver_user_id)
                    {
                        echo "Inbox Message"; ?>
                        <a href="<?= Url::to(['index'])?>" class="pull-right go-back"> <i class="fa fa-angle-left"></i> Go back </a>
                <?php  } 
                else
                    {
                        echo "Sent Message"; ?>
                        <a href="<?= Url::to(['sent'])?>" class="pull-right go-back"> <i class="fa fa-angle-left"></i> Go back </a>
                <?php    }
                ?>
                
                </h3>
            </div>
   
            <div class="panel-body">
                <div class="media">
                    <div class="media-body">
                        <span class="media-meta pull-right">
                        <?php 
                        $now = new DateTime();
                        $selecteddate=date("d/m/y ",  strtotime($model->created_at));
                        $currentdate=$now->format('d/m/y');
                        if($currentdate > $selecteddate)
                            {
                                echo date("d M ",  strtotime($model->created_at));
                            }
                        else
                            {
                                 echo date(" H:i A",  strtotime($model->created_at));
                            }
                        ?>
                        </span>
                        
                        <?php if(Yii::$app->user->id == $model->receiver_user_id)
                            {
                                // echo '<h4 class="message-sender-name">'.$user->username.'</h4>';
                                echo  '<small class="text-muted">From: '.$user->username.'</small>';
                            } 
                            else
                            {
                                // echo '<h4 class="message-sender-name">'.$user->username.'</h4>';
                                echo  '<small class="text-muted">To: '. $userreceiver->username.'</small>';
                            }
                        ?>
                       
                    </div>
                </div>
                <hr>
                <!-- media -->
    
                <p><?php echo $model->message_text; ?></p><br>

                <p> Thank you, <br>
                <?php  echo $user->username;  ?></p>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-toolbar message-controls" role="toolbar">
                            <div class="pull-right">
                                <!-- <button type="button" class="btn btn-transparent" data-toggle="tooltip" data-placement="bottom" title="Save this message"><i class="fa fa-floppy-o"></i></button> -->
                                <?= Html::a('', ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-transparent fa fa-trash-o',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]) ?>
                                <a href="<?= Url::to(['create','id'=>$model->sender_user_id])?>" class="btn btn-transparent"> <span>Reply</span> <i class="fa fa-reply"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
           

            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- end Col-9 -->
</div>