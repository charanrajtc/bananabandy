<?php

namespace frontend\models;

use Yii;
use common\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $sender_user_id
 * @property integer $receiver_user_id
 * @property string $message_text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $receiverUser
 * @property User $senderUser
 */
class Message extends \yii\db\ActiveRecord
{

    const EVENT_MESSAGE_SENT = 'message-sent';
    const EVENT_MESSAGE_BADWORDS_DENIED='message-badwords-denied';

    public $username;

    //initialize the events 
    public function init(){

        // attach the events for logging 
        $this->on(self::EVENT_MESSAGE_SENT, [$this, 'logEventMessage']);
        $this->on(self::EVENT_MESSAGE_BADWORDS_DENIED, [$this, 'logEventMessage']);
        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_user_id', 'receiver_user_id', 'message_text',], 'required'],
            [['sender_user_id', 'receiver_user_id','isred'], 'integer'],
            [['message_text'], 'string'],
            
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'exist', 'targetClass' => '\common\models\User', 'targetAttribute' => 'username','message' => 'This username does not exist.'],
            [['created_at', 'updated_at'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date("Y-m-d H:i:s"); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_user_id' => 'Sender User ID',
            'receiver_user_id' => 'Receiver User ID',
            'message_text' => 'Message Text',
            'isred'=> 'Is Red',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiverUser()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderUser()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_user_id']);
    }

    public function logEventMessage($event){
      Yii::info('recived message event for logging : '.$event->name);
    }
}
