### Backend-developer Test Bananabandy 

Assignement [pdf]() 
Anagrams.php Assignemnt - 1 [demo](http://coresoft.co.in/web/bananabandy/Anagrams.php) 
[source](https://bitbucket.org/charanrajtc/bananabandy)

Yii2 Test Demo [fornt-end Url](http://coresoft.co.in/web/bananabandy/yiiAPP/frontend/web/index.php)
Yii2 Test Demo [back-end Url](http://coresoft.co.in/web/bananabandy/yiiAPP/backend/web/index.php)

@author: [Charan Raj T C](http://coresoft.co.in/charanraj.tc/) `charanraj.tc@gmail.com`	

### some notable implentation  

1. Simple messagening between users 
2. Logging of messages using events and yii inbuilt logger 
3. Notification of messages to the user using javascript polling 
4. RBAC for the entire site and also user defined permissions
6. Simple Bad words filter in the messages using sql to full text search 

## Installition Instructions 

1. Install [composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
2. clone the Repo `git clone https://charanrajtc@bitbucket.org/charanrajtc/bananabandy.git`
3. checkout to developemnt branch `git checkout developemnt` 
4. Change the working directory `cd yiiAPP`
5. Initialtize the app `php init --env=Development --overwrite=No` 
6. Install Dependencies `php composer.phar install`
7. Configure or remove db array in database in common/config/main-local.php 
8. Migrate the Database `php yii migrate`  select all 
9. RABC migrations `php yii migrate --migrationPath=@yii/rbac/migrations`
10. Add roles and permissions for default users `php yii rbac/init`

   

## Predefined users 

1. username : joeUser , password : joe 
2. username : anniePrivileged , password : annie
3. username : robAdmin , password : rob

## Defined roles 

1. user : Access Front-end application : allows sending of meesages restricted to bad words filter
2. privilaged : Access Front-end application : allows sending of restricted meesages and other permissions of user role 
3. admin : Access Backend and Front-end of  application :  managing users , managing bad words and other permissions of privilaged role 